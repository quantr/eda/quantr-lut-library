package hk.quantr.lutlibrary.structure.output;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class LUT {

	ArrayList<LUT> input = new ArrayList();
	ArrayList<LUT> output = new ArrayList();

	public int getNoOfIntput() {
		return input.size();
	}

	public int getNoOfOutput() {
		return output.size();
	}
}
