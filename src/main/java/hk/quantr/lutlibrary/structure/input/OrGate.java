package hk.quantr.lutlibrary.structure.input;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class OrGate extends Vertex {

	public OrGate(String name) {
		super(name, 2, 1);
	}

}
