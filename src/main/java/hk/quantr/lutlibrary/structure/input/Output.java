package hk.quantr.lutlibrary.structure.input;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Output {

	public String name;
	public Input input;

	public Output(String name) {
		this.name = name;
	}

}
