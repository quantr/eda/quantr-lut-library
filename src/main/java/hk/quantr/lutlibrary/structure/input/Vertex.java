package hk.quantr.lutlibrary.structure.input;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public abstract class Vertex {

	public String name;
	public ArrayList<Input> inputs = new ArrayList();
	public ArrayList<Output> outputs = new ArrayList();

	public Vertex(String name, int inputPins, int outputPins) {
		this.name = name;
		for (int i = 0; i < inputPins; i++) {
			Input in = new Input("input " + i);
			inputs.add(in);
		}
		for (int i = 0; i < outputPins; i++) {
			Output output = new Output("output " + i);
			outputs.add(output);
		}
	}

}
