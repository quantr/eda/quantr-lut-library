package hk.quantr.lutlibrary.structure.input;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class AndGate extends Vertex {

	public AndGate(String name) {
		super(name, 2, 1);
	}

}
