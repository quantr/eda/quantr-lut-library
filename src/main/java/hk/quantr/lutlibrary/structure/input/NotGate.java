package hk.quantr.lutlibrary.structure.input;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class NotGate extends Vertex {

	public NotGate(String name) {
		super(name, 1, 1);
	}

}
