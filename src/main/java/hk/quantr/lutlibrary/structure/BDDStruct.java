/*
 * Copyright 2024 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.lutlibrary.structure;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author michelle
 */
public class BDDStruct<T> {

	public BDDFactory factory;
	public HashMap<T, BDD> variableMap = new HashMap<>();
	public int varCount;

	public BDDStruct(int numVariables, int cacheSize, ArrayList<T> vars) {
		factory = BDDFactory.init("javabdd", numVariables, cacheSize);
		for (int varCount = 0; varCount < vars.size(); varCount++) {
			variableMap.put(vars.get(varCount), factory.ithVar(varCount));
		}
	}

	public BDDStruct(int numVariables, int cacheSize) {
		factory = BDDFactory.init("javabdd", numVariables, cacheSize);
		factory.setVarNum(100);
		varCount = 0;
	}

	public void addVar(T var) {
		if (!variableMap.containsKey(var)) {
			BDD temp = factory.ithVar(varCount);
			variableMap.put(var, temp);
			varCount++;
		}
	}

	public void addSubBdd(T var, BDD bdd) {
		if (!variableMap.containsKey(var)) {
			variableMap.put(var, bdd);
		}
	}
}
