/*
 * Copyright 2024 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.lutlibrary.structure;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author michelle
 */
public class BDD {

// Define terminal nodes as static members
	public static final BDDNode TERMINAL_0 = new BDDNode(-1, null, null);
	public static final BDDNode TERMINAL_1 = new BDDNode(-1, new BDDNode(0, null, null), null);

	public BDDNode apply(BDDNode f, BDDNode g, String operation) {
		if (f == null || (g == null && !operation.equals("NOT"))) {
			throw new IllegalArgumentException("Input BDDs cannot be null");
		}
		return applyRecursive(f, g, operation);
	}

	private BDDNode applyRecursive(BDDNode f, BDDNode g, String operation) {
// Terminal cases:
		if (f.variable == -1) { // f is a terminal node (constant)
			if (operation.equals("NOT")) {
				return (f == TERMINAL_0) ? TERMINAL_1 : TERMINAL_0; // Use the defined terminal nodes
			} else {
				return applyConstantToBDD(f, g, operation);
			}
		} else if (g != null && g.variable == -1) { // g is a terminal node (constant)
			return applyConstantToBDD(g, f, operation);
		}

// Non-terminal cases:
		if (operation.equals("NOT")) { // Special handling for NOT
			BDDNode low = applyRecursive(f.low, null, operation);
			BDDNode high = applyRecursive(f.high, null, operation);
			return new BDDNode(f.variable, low, high);
		} else if (f.variable == g.variable) {
			BDDNode low = applyRecursive(f.low, g.low, operation);
			BDDNode high = applyRecursive(f.high, g.high, operation);
			return new BDDNode(f.variable, low, high);
		} else if (f.variable < g.variable) {
			BDDNode low = applyRecursive(f.low, g, operation);
			BDDNode high = applyRecursive(f.high, g, operation);
			return new BDDNode(f.variable, low, high);
		} else { // f.variable > g.variable
			BDDNode low = applyRecursive(f, g.low, operation);
			BDDNode high = applyRecursive(f, g.high, operation);
			return new BDDNode(g.variable, low, high);
		}
	}

	private BDDNode applyConstantToBDD(BDDNode constant, BDDNode bdd, String operation) {
		if (operation.equals("AND")) {
			return (constant == TERMINAL_0) ? TERMINAL_0 : bdd; // AND with 0 is 0, else it's the BDD
		} else if (operation.equals("OR")) {
			return (constant == TERMINAL_0) ? bdd : TERMINAL_1; // OR with 1 is 1, else it's the BDD
		} else if (operation.equals("XOR")) {
			return (constant == TERMINAL_0) ? bdd : applyRecursive(bdd, TERMINAL_1, "NOT"); // XOR with 1 is NOT BDD
		} else if (operation.equals("NOT")) {
			return applyRecursive(bdd, null, "NOT");
		} else {
			throw new IllegalArgumentException("Invalid operation: " + operation);
		}
	}

	public String toDot(BDDNode node) {
		StringBuilder sb = new StringBuilder();
		sb.append("digraph BDD {\n");
		sb.append("  rankdir=TB;\n");
		sb.append("  node [shape=circle];\n");
		toDotRecursive(node, sb, new StringBuilder());
		sb.append("}\n");
		return sb.toString();
	}

	private void toDotRecursive(BDDNode node, StringBuilder sb, StringBuilder visited) {
		if (node == null || visited.indexOf("|" + node.hashCode() + "|") != -1) {
			return;
		}
		visited.append("|").append(node.hashCode()).append("|");
		if (node.variable == -1) {
			sb.append("  ").append(node.hashCode()).append(" [label=\"").append(node.low != null ? "1" : "0").append("\"];\n");
		} else {
			sb.append("  ").append(node.hashCode()).append(" [label=\"x").append(node.variable).append("\"];\n");
			toDotRecursive(node.low, sb, visited);
			toDotRecursive(node.high, sb, visited);
			sb.append("  ").append(node.hashCode()).append(" -> ").append(node.low.hashCode()).append(" [style=dotted];\n");
			sb.append("  ").append(node.hashCode()).append(" -> ").append(node.high.hashCode()).append(" [style=solid];\n");
		}
	}

	public static void main(String[] args) {
		BDDNode a = new BDDNode(1, TERMINAL_0, TERMINAL_1); // Variable a
		BDDNode b = new BDDNode(2, TERMINAL_0, TERMINAL_1); // Variable b
		BDDNode c = new BDDNode(3, TERMINAL_0, TERMINAL_1); // Variable c
// Applying operations
		BDD bdd = new BDD();
		BDDNode andResult = bdd.apply(a, b, "AND");
		BDDNode result2 = bdd.apply(a, c, "AND");
		BDDNode result = bdd.apply(andResult, result2, "OR");
		try (FileWriter fileWriter = new FileWriter("dot.txt")) {
			fileWriter.write(bdd.toDot(result));
		} catch (IOException e) {
			System.out.println("An error occurred while writing to the file: " + e.getMessage());
		}
	}
}
