/*
 * Copyright 2024 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.lutlibrary.structure;

/**
 *
 * @author michelle
 */
public class BDDNode {

	public int variable; 
	public BDDNode low;  // Branch for variable = false
	public BDDNode high; // Branch for variable = true

	public BDDNode(int variable, BDDNode low, BDDNode high) {
		this.variable = variable;
		this.low = low;
		this.high = high;
	}
}
