<data>
  <modules>
    <entry>
      <string>Main</string>
      <module>
        <name>Main</name>
        <vertices>
          <pin>
            <outputs>
              <output>
                <parent class="pin" reference="../../.."/>
                <name>Pin 0 output</name>
                <deltaX>2</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>20</x>
                      <y>12</y>
                    </start>
                    <end>
                      <x>24</x>
                      <y>12</y>
                    </end>
                    <name>wire Point{x=20, y=12}toPoint{x=24, y=12}Pin 0 output, 2 / 1 null</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <ports/>
                <dataIndex>-1</dataIndex>
              </output>
            </outputs>
            <isSelected>false</isSelected>
            <gridSize>10</gridSize>
            <name>Pin 0</name>
            <inputs/>
            <properties>
              <entry>
                <string>Label</string>
                <string></string>
              </entry>
              <entry>
                <string>Name</string>
                <string>Pin 0</string>
              </entry>
              <entry>
                <string>Data Bits</string>
                <int>1</int>
              </entry>
              <entry>
                <string>Orientation</string>
                <string>east</string>
              </entry>
              <entry>
                <string>Radix</string>
                <string>bin</string>
              </entry>
              <entry>
                <string>Value</string>
                <long>0</long>
              </entry>
            </properties>
            <x>18</x>
            <y>11</y>
            <width>2</width>
            <height>2</height>
            <level>0</level>
            <showLevel>false</showLevel>
            <simTime>0</simTime>
            <initX>-1</initX>
            <initY>-1</initY>
            <radix>bin</radix>
            <base>2</base>
            <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
            <hex>0000000000000000</hex>
            <dec>00000000000000000000</dec>
          </pin>
          <pin>
            <outputs>
              <output>
                <parent class="pin" reference="../../.."/>
                <name>Pin 1 output</name>
                <deltaX>2</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>20</x>
                      <y>17</y>
                    </start>
                    <end>
                      <x>24</x>
                      <y>17</y>
                    </end>
                    <name>wire Point{x=20, y=17}toPoint{x=24, y=17}Pin 1 output, 2 / 1 null</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <ports/>
                <dataIndex>-1</dataIndex>
              </output>
            </outputs>
            <isSelected>false</isSelected>
            <gridSize>10</gridSize>
            <name>Pin 1</name>
            <inputs/>
            <properties>
              <entry>
                <string>Label</string>
                <string></string>
              </entry>
              <entry>
                <string>Name</string>
                <string>Pin 1</string>
              </entry>
              <entry>
                <string>Data Bits</string>
                <int>1</int>
              </entry>
              <entry>
                <string>Orientation</string>
                <string>east</string>
              </entry>
              <entry>
                <string>Radix</string>
                <string>bin</string>
              </entry>
              <entry>
                <string>Value</string>
                <long>0</long>
              </entry>
            </properties>
            <x>18</x>
            <y>16</y>
            <width>2</width>
            <height>2</height>
            <level>0</level>
            <showLevel>false</showLevel>
            <simTime>0</simTime>
            <initX>-1</initX>
            <initY>-1</initY>
            <radix>bin</radix>
            <base>2</base>
            <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
            <hex>0000000000000000</hex>
            <dec>00000000000000000000</dec>
          </pin>
          <pin>
            <outputs>
              <output>
                <parent class="pin" reference="../../.."/>
                <name>Pin 2 output</name>
                <deltaX>2</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>20</x>
                      <y>23</y>
                    </start>
                    <end>
                      <x>34</x>
                      <y>23</y>
                    </end>
                    <name>wire Point{x=20, y=23}toPoint{x=34, y=23}Pin 2 output, 2 / 1 null</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <ports/>
                <dataIndex>-1</dataIndex>
              </output>
            </outputs>
            <isSelected>false</isSelected>
            <gridSize>10</gridSize>
            <name>Pin 2</name>
            <inputs/>
            <properties>
              <entry>
                <string>Label</string>
                <string></string>
              </entry>
              <entry>
                <string>Name</string>
                <string>Pin 2</string>
              </entry>
              <entry>
                <string>Data Bits</string>
                <int>1</int>
              </entry>
              <entry>
                <string>Orientation</string>
                <string>east</string>
              </entry>
              <entry>
                <string>Radix</string>
                <string>bin</string>
              </entry>
              <entry>
                <string>Value</string>
                <long>0</long>
              </entry>
            </properties>
            <x>18</x>
            <y>22</y>
            <width>2</width>
            <height>2</height>
            <level>0</level>
            <showLevel>false</showLevel>
            <simTime>0</simTime>
            <initX>-1</initX>
            <initY>-1</initY>
            <radix>bin</radix>
            <base>2</base>
            <bin>0000000000000000000000000000000000000000000000000000000000000000</bin>
            <hex>0000000000000000</hex>
            <dec>00000000000000000000</dec>
          </pin>
          <orGate>
            <outputs>
              <output>
                <parent class="orGate" reference="../../.."/>
                <name>OrGate 3 output</name>
                <deltaX>5</deltaX>
                <deltaY>2</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>32</x>
                      <y>15</y>
                    </start>
                    <end>
                      <x>34</x>
                      <y>15</y>
                    </end>
                    <name>wire Point{x=32, y=15}toPoint{x=34, y=15}OrGate 3 output, 5 / 2 null</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <ports/>
                <dataIndex>-1</dataIndex>
              </output>
            </outputs>
            <isSelected>false</isSelected>
            <gridSize>10</gridSize>
            <name>OrGate 3</name>
            <inputs>
              <input>
                <parent class="orGate" reference="../../.."/>
                <name>OrGate 3 input 0</name>
                <deltaX>0</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>24</x>
                      <y>14</y>
                    </start>
                    <end>
                      <x>27</x>
                      <y>14</y>
                    </end>
                    <name>wire Point{x=24, y=14}toPoint{x=27, y=14}null OrGate 3 input 0, 0 / 1</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <dataPortIndex>-1</dataPortIndex>
              </input>
              <input>
                <parent class="orGate" reference="../../.."/>
                <name>OrGate 3 input 1</name>
                <deltaX>0</deltaX>
                <deltaY>3</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>24</x>
                      <y>16</y>
                    </start>
                    <end>
                      <x>27</x>
                      <y>16</y>
                    </end>
                    <name>wire Point{x=24, y=16}toPoint{x=27, y=16}null OrGate 3 input 1, 0 / 3</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <dataPortIndex>-1</dataPortIndex>
              </input>
            </inputs>
            <properties>
              <entry>
                <string>Label</string>
                <string></string>
              </entry>
              <entry>
                <string>Name</string>
                <string>OrGate 3</string>
              </entry>
              <entry>
                <string>No. Of Inputs</string>
                <int>2</int>
              </entry>
              <entry>
                <string>Data Bits</string>
                <int>1</int>
              </entry>
              <entry>
                <string>Orientation</string>
                <string>east</string>
              </entry>
            </properties>
            <x>27</x>
            <y>13</y>
            <width>5</width>
            <height>4</height>
            <level>0</level>
            <showLevel>false</showLevel>
            <simTime>0</simTime>
            <initX>-1</initX>
            <initY>-1</initY>
          </orGate>
          <NandGate>
            <outputs>
              <output>
                <parent class="NandGate" reference="../../.."/>
                <name>NandGate 4 output</name>
                <deltaX>6</deltaX>
                <deltaY>2</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>44</x>
                      <y>19</y>
                    </start>
                    <end>
                      <x>49</x>
                      <y>19</y>
                    </end>
                    <name>wire Point{x=44, y=19}toPoint{x=49, y=19}NandGate 4 output, 6 / 2 Probe 5 input 0, 0 / 1</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <ports/>
                <dataIndex>-1</dataIndex>
              </output>
            </outputs>
            <isSelected>false</isSelected>
            <gridSize>10</gridSize>
            <name>NandGate 4</name>
            <inputs>
              <input>
                <parent class="NandGate" reference="../../.."/>
                <name>NandGate 4 input 0</name>
                <deltaX>0</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>34</x>
                      <y>18</y>
                    </start>
                    <end>
                      <x>38</x>
                      <y>18</y>
                    </end>
                    <name>wire Point{x=34, y=18}toPoint{x=38, y=18}null NandGate 4 input 0, 0 / 1</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <dataPortIndex>-1</dataPortIndex>
              </input>
              <input>
                <parent class="NandGate" reference="../../.."/>
                <name>NandGate 4 input 1</name>
                <deltaX>0</deltaX>
                <deltaY>3</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge>
                    <start>
                      <x>34</x>
                      <y>20</y>
                    </start>
                    <end>
                      <x>38</x>
                      <y>20</y>
                    </end>
                    <name>wire Point{x=34, y=20}toPoint{x=38, y=20}null NandGate 4 input 1, 0 / 3</name>
                  </edge>
                  <edge reference="../edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <dataPortIndex>-1</dataPortIndex>
              </input>
            </inputs>
            <properties>
              <entry>
                <string>Label</string>
                <string></string>
              </entry>
              <entry>
                <string>Name</string>
                <string>NandGate 4</string>
              </entry>
              <entry>
                <string>No. Of Inputs</string>
                <int>2</int>
              </entry>
              <entry>
                <string>Data Bits</string>
                <int>1</int>
              </entry>
              <entry>
                <string>Orientation</string>
                <string>east</string>
              </entry>
            </properties>
            <x>38</x>
            <y>17</y>
            <width>6</width>
            <height>4</height>
            <level>0</level>
            <showLevel>false</showLevel>
            <simTime>0</simTime>
            <initX>-1</initX>
            <initY>-1</initY>
          </NandGate>
          <probe>
            <outputs/>
            <isSelected>false</isSelected>
            <gridSize>10</gridSize>
            <name>Probe 5</name>
            <inputs>
              <input>
                <parent class="probe" reference="../../.."/>
                <name>Probe 5 input 0</name>
                <deltaX>0</deltaX>
                <deltaY>1</deltaY>
                <x>0</x>
                <y>0</y>
                <edges>
                  <edge reference="../../../../../NandGate/outputs/output/edges/edge"/>
                  <edge reference="../../../../../NandGate/outputs/output/edges/edge"/>
                </edges>
                <connected>false</connected>
                <level>-1</level>
                <state>off</state>
                <bits>1</bits>
                <value>0</value>
                <preValue>-1</preValue>
                <ref></ref>
                <vcdDefined>false</vcdDefined>
                <dataPortIndex>-1</dataPortIndex>
              </input>
            </inputs>
            <properties>
              <entry>
                <string>Label</string>
                <string></string>
              </entry>
              <entry>
                <string>Name</string>
                <string>Probe 5</string>
              </entry>
              <entry>
                <string>Orientation</string>
                <string>west</string>
              </entry>
              <entry>
                <string>Radix</string>
                <string>dec</string>
              </entry>
            </properties>
            <x>49</x>
            <y>18</y>
            <width>2</width>
            <height>2</height>
            <level>0</level>
            <showLevel>false</showLevel>
            <simTime>0</simTime>
            <initX>-1</initX>
            <initY>-1</initY>
          </probe>
        </vertices>
        <edges>
          <edge reference="../../vertices/pin/outputs/output/edges/edge"/>
          <edge>
            <start>
              <x>24</x>
              <y>12</y>
            </start>
            <end>
              <x>24</x>
              <y>14</y>
            </end>
            <name>wire Point{x=24, y=12}toPoint{x=24, y=14}null null</name>
          </edge>
          <edge reference="../../vertices/orGate/inputs/input/edges/edge"/>
          <edge reference="../../vertices/orGate/inputs/input[2]/edges/edge"/>
          <edge>
            <start>
              <x>24</x>
              <y>16</y>
            </start>
            <end>
              <x>24</x>
              <y>17</y>
            </end>
            <name>wire Point{x=24, y=16}toPoint{x=24, y=17}null null</name>
          </edge>
          <edge reference="../../vertices/pin[2]/outputs/output/edges/edge"/>
          <edge reference="../../vertices/pin[3]/outputs/output/edges/edge"/>
          <edge>
            <start>
              <x>34</x>
              <y>20</y>
            </start>
            <end>
              <x>34</x>
              <y>23</y>
            </end>
            <name>wire Point{x=34, y=20}toPoint{x=34, y=23}null null</name>
          </edge>
          <edge reference="../../vertices/NandGate/inputs/input[2]/edges/edge"/>
          <edge reference="../../vertices/orGate/outputs/output/edges/edge"/>
          <edge>
            <start>
              <x>34</x>
              <y>15</y>
            </start>
            <end>
              <x>34</x>
              <y>18</y>
            </end>
            <name>wire Point{x=34, y=15}toPoint{x=34, y=18}null null</name>
          </edge>
          <edge reference="../../vertices/NandGate/inputs/input/edges/edge"/>
          <edge reference="../../vertices/NandGate/outputs/output/edges/edge"/>
        </edges>
      </module>
    </entry>
  </modules>
</data>