/*
 * Copyright 2024 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author michelle
 */
import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import com.github.javabdd.JFactory;

import java.util.*;

public class TestCutBDD {

    private BDDFactory factory;
    private Map<String, Integer> variableIndexMap;
    private int varCount;
    private int lutInputSize;

    public TestCutBDD(int lutInputSize) {
        factory = JFactory.init(1000, 100);
        variableIndexMap = new HashMap<>();
        varCount = 0;
        this.lutInputSize = lutInputSize;
    }

    public BDD buildBDD(String[] expressions) {
        int variableCounter = 0;
        for (String expr : expressions) {
            String[] variables = expr.split(" AND ");
            for (String var : variables) {
                if (!variableIndexMap.containsKey(var)) {
                    variableIndexMap.put(var, variableCounter++);
                }
            }
        }

        factory.setVarNum(variableIndexMap.size());

        BDD result = factory.zero();
        for (String expr : expressions) {
            String[] variables = expr.split(" AND ");
            BDD clause = factory.one();

            for (String var : variables) {
                int varIndex = variableIndexMap.get(var);
                clause.andWith(factory.ithVar(varIndex));
            }
            result.orWith(clause);
        }

        return result;
    }

    public List<BDD> cutBDD(BDD bdd) {
        List<BDD> luts = new ArrayList<>();
        Queue<BDD> queue = new LinkedList<>();

        queue.add(bdd);

        while (!queue.isEmpty()) {
            BDD currentBDD = queue.poll();

            if (currentBDD.var() < 0 || currentBDD.var() >= lutInputSize) { // base case
                luts.add(currentBDD.id()); 
            } else {
                BDD high = currentBDD.high();
                BDD low = currentBDD.low();

                if (high.var() != -1) { // -1 means terminal node
                    queue.add(high);
                } else {
                    luts.add(high.id());
                }

                if (low.var() != -1) {
                    queue.add(low);
                } else {
                    luts.add(low.id());
                }
            }
        }

        return luts;
    }

    
    public void releaseResources() {
        factory.done();
    }

    public static void main(String[] args) {
        TestBDD builder = new TestBDD();

        // Example usage: (a AND b) OR (c AND d)
        String [] expressions = {"a AND c", "c AND d"};
        BDD bdd = builder.buildBDD(expressions);

        System.out.println(bdd);

        bdd.free();
        builder.releaseResources();
    }
}
