
import hk.quantr.lutlibrary.structure.input.AndGate;
import hk.quantr.lutlibrary.structure.input.OrGate;
import java.io.IOException;
import org.junit.Test;
import com.github.javabdd.*;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestLutLibrary {

    int numVariables = 3; 
    int cacheSize = 10000;  
    BDDFactory factory = BDDFactory.init("javabdd", numVariables, cacheSize);

    public BDD getSubBDD(BDD bdd, int varIndex) {
//		System.out.println(factory.varNum());
        if (varIndex >= factory.varNum()) {
            throw new IllegalArgumentException("Variable index is out of bounds");
        }
        BDD high = bdd.restrict(factory.ithVar(varIndex));

     
        BDD low = bdd.restrict(factory.nithVar(varIndex));

      
        BDD subBDD = factory.ithVar(varIndex).ite(high, low);

        return subBDD;
    }

    @Test
    public void test() throws IOException {
        OrGate or1 = new OrGate("Or 1");
        AndGate and1 = new AndGate("And 1");
        AndGate and2 = new AndGate("And 2");
        or1.outputs.get(0).input = and1.inputs.get(0);
        and1.outputs.get(0).input = and1.inputs.get(1);
        factory.setVarNum(numVariables);
        factory.enableReorder();
        BDD a = factory.ithVar(0); // Variable a
        BDD b = factory.ithVar(1); // Variable b
        BDD c = factory.ithVar(2); // Variable c

       
        BDD bandc = b.and(c);

       
        BDD aandb = a.and(b);

        
        BDD result = bandc.or(aandb);


        int[] order = new int[]{1, 2, 0}; // can change according to the minimal number of nodes

        BDD subBDD = getSubBDD(result, 2);
//		System.out.println(subBDD.toString());

        // Apply new ordering
        factory.setVarOrder(order);
//		factory.done();
        System.out.println(result.toString());

		result.printSet();
		result.printDot();
//		result.printSetWithDomains();
//		String str = IOUtils.toString(new FileInputStream("src/test/resources/test1.ql"), "utf-8");
//		QuantrLUTLibrary.convertToLUT(str);
    }
}
