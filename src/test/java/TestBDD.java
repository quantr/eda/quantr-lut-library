/*
 * Copyright 2024 michelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @author michelle
 */
import com.github.javabdd.BDD;
import com.github.javabdd.BDDFactory;
import com.github.javabdd.JFactory;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;

public class TestBDD {

    private BDDFactory factory;
    private Map<String, Integer> variableIndexMap;
    private int varCount;

    public TestBDD() {
        factory = JFactory.init(1000, 100);
        variableIndexMap = new HashMap<>();
        varCount = 0;
    }

    private BDD getVariableBDD(String var) {
        // Check if the variable is negated with NOT
        boolean isNegated = var.startsWith("NOT ");
        if (isNegated) {
            var = var.substring(4);
        }

        // Add variable to the map if it's not there
        variableIndexMap.putIfAbsent(var, varCount++);

        // Get the BDD representation of the variable
        BDD varBDD = factory.ithVar(variableIndexMap.get(var));

        // If the variable is negated, return the negation of the BDD
        return isNegated ? varBDD.not() : varBDD;
    }

    public BDD buildBDD(String[] expressions) {
        int variableCounter = 0;
        for (String expr : expressions) {
            String[] variables = expr.split(" AND ");
            for (String var : variables) {
                if (!variableIndexMap.containsKey(var)) {
                    variableIndexMap.put(var, variableCounter++);
                }
            }
        }

        factory.setVarNum(variableIndexMap.size());

        BDD result = factory.zero();
        for (String expr : expressions) {
            String[] variables = expr.split(" AND ");
            BDD clause = factory.one();

            for (String var : variables) {
                int varIndex = variableIndexMap.get(var);
                clause.andWith(factory.ithVar(varIndex));
            }
            result.orWith(clause);
        }

        return result;
    }

//    public BDD buildBDD(String[] expressions) {
//        // Set the number of variables in the factory
//        factory.setVarNum(variableIndexMap.size());
//
//        BDD result = factory.zero();
//
//        for (String expr : expressions) {
//            BDD clause = factory.one();
//            
//            // Split the expression on OR if present
//            String[] orExpressions = expr.split(" OR ");
//            for (String orExpr : orExpressions) {
//                BDD term = factory.zero();
//
//                // Split the terms on AND
//                String[] andExpressions = orExpr.trim().split(" AND ");
//                for (String andExpr : andExpressions) {
//                    BDD variableBDD = getVariableBDD(andExpr.trim());
//                    term.orWith(variableBDD);
//                }
//
//                // AND the terms with the current clause
//                clause.andWith(term);
//            }
//
//            // OR the clause with the result
//            result.orWith(clause);
//        }
//
//        return result;
//    }
    public void releaseResources() {
        factory.done();
    }

    public static void main(String[] args) {
        TestBDD builder = new TestBDD();

        // Example usage: (a AND b) OR (c AND d)
//        String aa = JOptionPane.showInputDialog("Enter boolean expression: ");
//        System.out.println(aa);
//        String[] expressions = aa.split(",");
       String[] expression = {"a AND b AND c", "b AND c", "a AND d AND e"};
        BDD bdd = builder.buildBDD(expression);

        System.out.println(bdd);
        bdd.printDot();
        bdd.free();
        builder.releaseResources();
    }
}
